<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests;

use DocsDispatcherIo\Sdk\Authentication\AuthenticationInterface;
use DocsDispatcherIo\Sdk\Client;
use DocsDispatcherIo\Sdk\ServiceMediator;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
  /**
   * @var Client
   */
  protected $client;

  private $authentication;

  /**
   * @inheritDoc
   */
  protected function setUp(): void
  {
    $this->authentication = $this->createMock(AuthenticationInterface::class);

    $this->client = new Client($this->authentication);
  }

  public function testExecuteRequestWithoutService()
  {
    $serviceMediator = $this->createMock(ServiceMediator::class);

    $serviceMediator->expects($this->once())->method('count')->willReturn(0);

    $this->expectException(\InvalidArgumentException::class);
    $this->client->executeRequest($serviceMediator, 'application/json');
  }

  public function testExecuteRequestWithClientException()
  {
    $serviceMediator = $this->createMock(ServiceMediator::class);
    $guzzleClient = $this->createMock(GuzzleClient::class);

    $serviceMediator->expects($this->once())->method('count')->willReturn(1);
    $serviceMediator->expects($this->once())->method('getEndpoint')->willReturn('/api/service-endpoint');
    $serviceMediator->expects($this->once())->method('getPayload')->willReturn(['some' => 'payload']);

    $client = $this->getMockBuilder(get_class($this->client))
      ->setConstructorArgs([$this->authentication])
      ->onlyMethods(['buildHttpClient', 'buildOptionsFromPayload', 'normalizeClientException'])
      ->getMock();

    $client->expects($this->once())->method('buildHttpClient')->willReturn($guzzleClient);
    $client->expects($this->once())->method('buildOptionsFromPayload')->willReturn(['some' => 'payload']);
    $this->authentication->expects($this->once())
      ->method('authenticate')
      ->with(
        $this->equalTo($guzzleClient),
        $this->equalTo('/api/service-endpoint'),
        $this->equalTo(['some' => 'payload'])
      );

    $clientException = new ClientException('No good', $this->createMock(Request::class));
    $guzzleClient->expects($this->once())
      ->method('__call')
      ->with(
        $this->equalTo('post'),
        $this->equalTo([
          '/api/service-endpoint',
          ['some' => 'payload']
        ])
      )
      ->willThrowException($clientException);

    $client->expects($this->once())->method('normalizeClientException')->with($this->equalTo($clientException))->willReturn($this->createMock(\Throwable::class));

    $this->expectException(\Throwable::class);
    $client->executeRequest($serviceMediator, 'application/json');
  }

  public function testExecuteRequest()
  {
    $serviceMediator = $this->createMock(ServiceMediator::class);
    $guzzleClient = $this->createMock(GuzzleClient::class);
    $response = $this->createMock(Response::class);

    $serviceMediator->expects($this->once())->method('count')->willReturn(1);
    $serviceMediator->expects($this->once())->method('getEndpoint')->willReturn('/api/service-endpoint');
    $serviceMediator->expects($this->once())->method('getPayload')->willReturn(['some' => 'payload']);

    $client = $this->getMockBuilder(get_class($this->client))
      ->setConstructorArgs([$this->authentication])
      ->onlyMethods(['buildHttpClient', 'buildOptionsFromPayload'])
      ->getMock();

    $client->expects($this->once())->method('buildHttpClient')->willReturn($guzzleClient);
    $client->expects($this->once())->method('buildOptionsFromPayload')->willReturn(['some' => 'payload']);
    $this->authentication->expects($this->once())
      ->method('authenticate')
      ->with(
        $this->equalTo($guzzleClient),
        $this->equalTo('/api/service-endpoint'),
        $this->equalTo(['some' => 'payload'])
      );

    $guzzleClient->expects($this->once())
      ->method('__call')
      ->with(
        $this->equalTo('post'),
        $this->equalTo([
          '/api/service-endpoint',
          ['some' => 'payload']
        ])
      )
      ->willReturn($response);

    $this->assertSame($response, $client->executeRequest($serviceMediator, 'application/json'));
  }

  public function testBuildOptionsFromPayload()
  {
    $reflectionMethod = new \ReflectionMethod($this->client, 'buildOptionsFromPayload');
    $reflectionMethod->setAccessible(true);

    $payload = ['some' => ['advanced' => 'data'], 'resultFileName' => 'test.pdf'];
    $accept = 'application/json';
    $result = $reflectionMethod->invoke($this->client, $payload, $accept);

    $this->assertSame([
      'headers' => [
        'Accept' => 'application/json'
      ],
      'json' => $payload,
      'sink' => 'test.pdf',
    ], $result);
  }
}
