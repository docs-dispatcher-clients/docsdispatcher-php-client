<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests\Argument;

use DocsDispatcherIo\Sdk\Argument\Enums\ESignRecipientRoles;
use DocsDispatcherIo\Sdk\Argument\Recipient;
use PHPUnit\Framework\TestCase;

class ESignRecipientTest extends TestCase
{
    protected $id = 'recipient1';
    protected $firstname = 'firstname';
    protected $name = 'name';
    protected $email = 'recipient@email.com';
    protected $phone = '+33265352448';
    protected $optionKey = 'key';
    protected $optionValue = 'value';
    protected $options = ['option1' => 'val1', 'option2' => 'val2'];
    protected $targetUser = 'user1@mail.com';
    protected $targetUser2 = 'user2@mail.com';

    public function testBuildPayload()
    {
        $recipient = (new Recipient(ESignRecipientRoles::WATCHER, $this->options))
            ->setId($this->id)
            ->setEmail($this->email)
            ->setName($this->name)
            ->setFirstname($this->firstname)
            ->setPhone($this->phone)
            ->addOption($this->optionKey, $this->optionValue)
            ->setTargetUsers([$this->targetUser])
            ->addTargetUser($this->targetUser2);
        $payload = $recipient->buildPayload();

        $this->assertEquals($this->id, $payload['id']);
        $this->assertEquals($this->firstname, $payload['firstname']);
        $this->assertEquals($this->name, $payload['name']);
        $this->assertEquals($this->email, $payload['email']);
        $this->assertEquals($this->phone, $payload['phone']);
        $this->assertEquals(ESignRecipientRoles::WATCHER, $payload['role']);

        $this->assertEquals(
            array_merge($this->options, [$this->optionKey => $this->optionValue]),
            $payload['options']
        );

        $this->assertContains($this->targetUser, $payload['targetUsers']);
        $this->assertContains($this->targetUser2, $payload['targetUsers']);
    }
}
