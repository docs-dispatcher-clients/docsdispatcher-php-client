<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests\Service;

use DocsDispatcherIo\Sdk\Service\UploadService;

class UploadServiceTest extends AbstractTargetableServiceBase
{
  /**
   * @inheritDoc
   */
  protected function setUp(): void
  {
    $this->targetableService = new UploadService(
      $this->templateName,
      $this->data,
      $this->resultFileName,
      [$this->getTarget1(), $this->getTarget2()]
    );
  }

  /**
   * @inheritDoc
   */
  protected function getExpectedEndPointName(): string
  {
    return 'upload';
  }
}
