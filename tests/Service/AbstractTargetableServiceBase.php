<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests\Service;

use DocsDispatcherIo\Sdk\Service\AbstractTargetableService;
use DocsDispatcherIo\Sdk\Tests\Utils\WithTargets;
use PHPUnit\Framework\TestCase;

abstract class AbstractTargetableServiceBase extends TestCase
{
  use WithTargets;

  /**
   * @var AbstractTargetableService
   */
  protected $targetableService;

  protected $templateName = 'testing-template';

  protected $data = ['some' => ['complex' => 'data']];
  protected $resultFileName = 'output.pdf';

  /**
   * @return string
   */
  abstract protected function getExpectedEndPointName(): string;

  public function testGetEndpointName()
  {
    $this->assertSame($this->getExpectedEndPointName(), $this->targetableService->getEndpointName());
  }

  public function testBuildPayload()
  {
    $generatedPayload = $this->targetableService->buildPayload();

    $this->assertSame($this->templateName, $generatedPayload['templateName']);
    $this->assertSame($this->getTargetsPayload(), $generatedPayload['targets']);
    $this->assertSame($this->data, $generatedPayload['data']);
  }

  public function testAddDatum()
  {
    $generatedPayload = $this->targetableService
      ->addDatum('1', 'new_value')
      ->buildPayload();

    $this->assertSame($this->templateName, $generatedPayload['templateName']);
    $this->assertSame($this->getTargetsPayload(), $generatedPayload['targets']);
    $this->assertNotSame($this->data, $generatedPayload['data']);

    $newData = $this->data;
    $newData['1'] = 'new_value';
    $this->assertSame($newData, $generatedPayload['data']);

    $generatedPayload = $this->targetableService
      ->setData(null)
      ->addDatum(1, 'new_value')
      ->buildPayload();
    $this->assertSame([1 => 'new_value'], $generatedPayload['data']);
  }

  public function testSetData()
  {
    $newFileName = 'file.pdf';
    $generatedPayload = $this->targetableService
      ->setData($this->data)
      ->setResultFileName($newFileName)
      ->buildPayload();

    $this->assertSame($this->templateName, $generatedPayload['templateName']);
    $this->assertSame($this->getTargetsPayload(), $generatedPayload['targets']);
    $this->assertSame($this->data, $generatedPayload['data']);
    $this->assertSame($newFileName, $generatedPayload['resultFileName']);
  }
}
