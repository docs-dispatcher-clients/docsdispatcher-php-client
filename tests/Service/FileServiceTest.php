<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests\Service;

use DocsDispatcherIo\Sdk\Argument\Enums\Targets;
use DocsDispatcherIo\Sdk\Argument\Target\ZohoCRMUploadTarget;
use DocsDispatcherIo\Sdk\Service\FileService;
use PHPUnit\Framework\TestCase;

class FileServiceTest extends TestCase
{
    /**
     * @var FileService
     */
    private $fileService;

    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        $this->fileService = new FileService('template-name', ['some' => ['complex' => 'data']], 'output.pdf');
    }

    public function testGetEndpointName()
    {
        $this->assertSame('file', $this->fileService->getEndpointName());
    }

    public function testBuildPayload()
    {
        $generatedPayload = $this->fileService->buildPayload();

        $this->assertSame('template-name', $generatedPayload['templateName']);
        $this->assertSame(['some' => ['complex' => 'data']], $generatedPayload['data']);
        $this->assertSame('output.pdf', $generatedPayload['resultFileName']);
        $this->assertArrayNotHasKey('targets', $generatedPayload);
    }

    public function testWithTargets()
    {
        $type = "Target-Type";
        $id = "1023";
        $target = new ZohoCRMUploadTarget($type, $id);

        $generatedPayload = $this->fileService->addTarget($target)->buildPayload();
        $this->assertNotEmpty($generatedPayload['targets']);
        $this->assertIsIterable($generatedPayload['targets']);
        $this->assertSame($id, $generatedPayload['targets'][0]['id']);
        $this->assertSame($type, $generatedPayload['targets'][0]['type']);
        $this->assertSame(Targets::ZOHO_CRM, $generatedPayload['targets'][0]['target']);
    }
}
