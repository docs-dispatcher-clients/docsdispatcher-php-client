<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests\Service;

use DocsDispatcherIo\Sdk\Argument\BasicRequest;
use DocsDispatcherIo\Sdk\Argument\Enums\ESignDeliveryTypes;
use DocsDispatcherIo\Sdk\Argument\Enums\ESignModes;
use DocsDispatcherIo\Sdk\Argument\Enums\ESignRecipientRoles;
use DocsDispatcherIo\Sdk\Argument\Enums\ESignTypes;
use DocsDispatcherIo\Sdk\Argument\ESignField;
use DocsDispatcherIo\Sdk\Argument\ESignFileContentRequest;
use DocsDispatcherIo\Sdk\Argument\Recipient;
use DocsDispatcherIo\Sdk\Service\ESignService;
use DocsDispatcherIo\Sdk\Tests\Utils\WithTargets;
use PHPUnit\Framework\TestCase;

class ESignServiceTest extends TestCase
{
    use WithTargets;

    public function testGetEndpointName()
    {
        $this->assertEquals('esign', (new ESignService([], []))->getEndpointName());
    }

    private function getRecipient(): Recipient
    {
        return (new Recipient())
            ->setFirstname('firstname');
    }

    private function getDocument(): ESignFileContentRequest
    {
        return (new ESignFileContentRequest())
            ->setTemplateName($this->templateName);
    }

    private function getFullDocument(): ESignFileContentRequest
    {
        return (new ESignFileContentRequest())
            ->setTemplateName($this->templateName)
            ->setResultFileName('output.pdf')
            ->setData(['some' => ['super' => 'content']])
            ->setTargets([$this->getTarget1()])
            ->addTarget($this->getTarget2());
    }

    protected $templateName = 'template-name';
    protected $message = 'simple static message';
    protected $messageSubject = 'simple subject message';
    protected $finalMessage = 'simple static final message';
    protected $finalMessageSubject = 'simple subject final message';

    public function testSimplePayloadWithFixedMessages()
    {
        $service = (new ESignService(
            $this->getRecipient(),
            $this->getDocument()
        ))
            ->setProvider('provider')
            ->setTargets($this->getTargets())
            ->setFixedMessage($this->messageSubject, $this->message)
            ->setFixedFinalDocMessage($this->finalMessageSubject, $this->finalMessage);

        $payload = $service->buildPayload();
        $this->assertEquals('provider', $payload['provider']);
        $this->assertCount(1, $payload['recipients']);
        $this->assertEquals('firstname', $payload['recipients'][0]['firstname']);
        $this->assertEquals(ESignRecipientRoles::SIGNER, $payload['recipients'][0]['role']);
        $this->assertCount(1, $payload['documents']);
        $this->assertEquals($this->templateName, $payload['documents'][0]['templateName']);
        $this->assertCount(2, $payload['targets']);

        $this->assertEquals($this->message, $payload['messageBody']);
        $this->assertEquals($this->messageSubject, $payload['subject']);
        $this->assertEquals($this->finalMessage, $payload['finalDocMessageBody']);
        $this->assertEquals($this->finalMessageSubject, $payload['finalDocMessageSubject']);
    }

    protected function getMessage(string $templateName): BasicRequest
    {
        return new BasicRequest($templateName);
    }

    protected $settings = ['some' => 'settings'];
    protected $recipientMail = 'a@b.c';

    public function testComplexPayload()
    {
        $service = (new ESignService(
            [$this->getRecipient()],
            [$this->getDocument()]
        ))
            ->addRecipient(new Recipient(ESignRecipientRoles::WATCHER))
            ->addDocument($this->getFullDocument())
            ->setProvider('provider')
            ->setTargets($this->getTargets())
            ->setMessage($this->getMessage('message-template'))
            ->setFinalDocMessage($this->getMessage('final-message-template'))
            ->setType(ESignTypes::ADVANCED)
            ->setFinalDocRecipients($this->getRecipient())
            ->addFinalDocRecipient(new Recipient(ESignRecipientRoles::SIGNER))
            ->setDeliveryType(ESignDeliveryTypes::EMAIL)
            ->setSigningMode(ESignModes::PARALLEL)
            ->setExpirationTime(1000)
            ->setSettings($this->settings)
            ->addSetting('key', 'value')
            ->addField(new ESignField($this->recipientMail));

        $payload = $service->buildPayload();

        $this->assertEquals($this->getTargetsPayload(), $payload['targets']);
        $this->assertEquals('message-template', $payload['message']['templateName']);
        $this->assertEquals(ESignRecipientRoles::WATCHER, $payload['recipients'][1]['role']);
        $this->assertEquals('final-message-template', $payload['finalDocMessage']['templateName']);
        $this->assertContains($this->getRecipient()->buildPayload(), $payload['finalDocRecipients']);
        $this->assertEquals('provider', $payload['provider']);
        $this->assertEquals(ESignTypes::ADVANCED, $payload['type']);
        $this->assertEquals(ESignDeliveryTypes::EMAIL, $payload['deliveryType']);
        $this->assertEquals(ESignModes::PARALLEL, $payload['signingMode']);
        $this->assertEquals(1000, $payload['expirationTime']);
        $this->assertEquals(array_merge($this->settings, ['key' => 'value']), $payload['settings']);
    }
}
