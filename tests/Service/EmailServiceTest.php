<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests\Service;

use DocsDispatcherIo\Sdk\Argument\Attachment;
use DocsDispatcherIo\Sdk\Service\EmailService;
use PHPUnit\Framework\TestCase;

class EmailServiceTest extends TestCase
{
    /**
     * @var EmailService
     */
    private $emailService;

    protected function setUp(): void
    {
        $this->emailService = new EmailService('sender@docs-dispatcher.io', 'receiver@docs-dispatcher.io', 'My subject', 'The body');
    }

    public function testGetEndpointName()
    {
        $this->assertSame('email', $this->emailService->getEndpointName());
    }

    public function testBuildPayload()
    {
        $cc = 'cc@docs-dispatcher.io';
        $bcc = 'bcc@docs-dispatcher.io';
        $templateName = 'some-random-template';
        $data = ['some' => 'data', 'more' => ['complex' => 'data']];
        $attachment = $this->createMock(Attachment::class);
        $attachmentPayload = [
            'templateName' => 'template-name',
        ];
        $attachment->expects($this->once())->method('buildPayload')->willReturn($attachmentPayload);

        $extraParams = [
            'cc' => [$cc],
            'bcc' => [$bcc],
            'templateName' => $templateName,
            'data' => $data,
            'attachments' => [
                $attachmentPayload
            ],
        ];

        $this->emailService->setCc($cc);
        $this->emailService->setBcc($bcc);
        $this->emailService->setTemplateName($templateName);
        $this->emailService->setData($data);
        $this->emailService->addAttachment($attachment);

        $payload = $this->emailService->buildPayload();

        foreach ($extraParams as $key => $value) {
            $this->assertArrayHasKey($key, $payload);
            $this->assertSame($value, $payload[$key]);
        }

        $this->assertArrayHasKey('from', $payload);
        $this->assertSame('sender@docs-dispatcher.io', $payload['from']);
        $this->assertArrayHasKey('to', $payload);
        $this->assertSame(['receiver@docs-dispatcher.io'], $payload['to']);
        $this->assertArrayHasKey('subject', $payload);
        $this->assertSame('My subject', $payload['subject']);
        $this->assertArrayHasKey('body', $payload);
        $this->assertSame('The body', $payload['body']);
    }
}

