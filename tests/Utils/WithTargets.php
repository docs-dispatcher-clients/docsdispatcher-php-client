<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests\Utils;

use DocsDispatcherIo\Sdk\Argument\AbstractTarget;
use DocsDispatcherIo\Sdk\Argument\Enums\Targets;
use DocsDispatcherIo\Sdk\Argument\Target\ZohoCRMUploadTarget;

trait WithTargets
{
    protected $type1 = 'Type 1';
    protected $id1 = '123';
    protected $type2 = 'Type 2';
    protected $id2 = '1234';

    protected function makeTarget(string $type, string $id): AbstractTarget
    {
        return new ZohoCRMUploadTarget($type, $id);
    }

    protected function getTarget1(): AbstractTarget
    {
        return $this->makeTarget($this->type1, $this->id1);
    }

    protected function getPayload1(): array
    {
        return [
            'target' => Targets::ZOHO_CRM,
            'type' => $this->type1,
            'id' => $this->id1,
        ];
    }

    protected function getTarget2(): AbstractTarget
    {
        return $this->makeTarget($this->type2, $this->id2);
    }

    protected function getTargets(): array
    {
        return [$this->getTarget1(), $this->getTarget2()];
    }

    protected function getPayload2(): array
    {
        return [
            'target' => Targets::ZOHO_CRM,
            'type' => $this->type2,
            'id' => $this->id2,
        ];
    }

    protected function getTargetsPayload(): array
    {
        return [
            $this->getPayload1(),
            $this->getPayload2(),
        ];
    }
}
