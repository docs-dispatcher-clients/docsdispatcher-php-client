<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests\Authentication;

use DocsDispatcherIo\Sdk\Authentication\BasicAuthAuthentication;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use PHPUnit\Framework\TestCase;

class BasicAuthAuthenticationTest extends TestCase
{
    /**
     * @var BasicAuthAuthentication
     */
    private $authentication;

    protected function setUp(): void
    {
        $this->authentication = new BasicAuthAuthentication('login', 'password');
    }

    public function testAuthenticate()
    {
        $guzzleClient = $this->createMock(Client::class);
        $endpointUrl = '/api/test';
        $options = [];

        $this->authentication->authenticate($guzzleClient, $endpointUrl, $options);

        $this->assertSame([RequestOptions::AUTH => ['login', 'password']], $options);
    }
}

