<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests;

use DocsDispatcherIo\Sdk\ServiceMediator;
use DocsDispatcherIo\Sdk\Service\ServiceInterface;
use PHPUnit\Framework\TestCase;

class ServiceMediatorTest extends TestCase
{
    public function testConstructor()
    {
        $service = $this->createMock(ServiceInterface::class);
        $serviceMediator = new ServiceMediator([$service]);

        $this->assertContains($service, $serviceMediator->getServices());
    }

    public function testAddService()
    {
        $service = $this->createMock(ServiceInterface::class);
        $serviceMediator = new ServiceMediator();

        $serviceMediator->addService($service);

        $this->assertContains($service, $serviceMediator->getServices());
    }

    public function testGetEndpointNoService()
    {
        $serviceMediator = new ServiceMediator();

        $this->expectException(\RuntimeException::class);
        $serviceMediator->getEndpoint();
    }

    public function testGetEndpoint()
    {
        $service = $this->createMock(ServiceInterface::class);
        $serviceMediator = new ServiceMediator([$service]);

        $service->expects($this->once())->method('getEndpointName')->willReturn('some-endpoint');

        $this->assertSame('/api/some-endpoint', $serviceMediator->getEndpoint());
    }

    public function testGetPayloadNoService()
    {
        $serviceMediator = new ServiceMediator();

        $this->expectException(\RuntimeException::class);
        $serviceMediator->getPayload();
    }

    public function testGetPayload()
    {
        $service1 = $this->createMock(ServiceInterface::class);
        $service2 = $this->createMock(ServiceInterface::class);
        $serviceMediator = new ServiceMediator([$service1, $service2]);

        $service1->expects($this->once())->method('buildPayload')->willReturn(['some' => ['cool' => 'payload']]);
        $service2->expects($this->once())->method('buildPayload')->willReturn(['other' => ['cooler' => 'payload']]);

        $this->assertSame(['some' => ['cool' => 'payload'], 'other' => ['cooler' => 'payload']], $serviceMediator->getPayload());
    }
}

