[![pipeline status](https://gitlab.com/docs-dispatcher-clients/docsdispatcher-php-client/badges/develop/pipeline.svg)](https://gitlab.com/docs-dispatcher-clients/docsdispatcher-php-client/commits/develop)
[![coverage report](https://gitlab.com/docs-dispatcher-clients/docsdispatcher-php-client/badges/develop/coverage.svg)](https://docs-dispatcher-clients.gitlab.io/docsdispatcher-php-client/coverage/)

# DocsDispatcher PHP Client

## Versions

| SDK version | PHP version | Branch                      |
| ----------- | ----------- | --------------------------- |
| v0.1.x      | 7.2+        | [php-7x](./-/tree/php-7x)   |
| v0.2.x      | 8.1+        | [develop](./-/tree/develop) |

## Installation

```
$ composer require docs-dispatcher.io/sdk
```

## Getting started

### Concepts

This library relays on the following concepts in order to be able to use it with a minimal knowledge of the API itself.

#### Authentication

Only supported provider is Basic Auth for now.

#### Service(s)

A service is an API endpoint having its own parameters. For a detailed list of them, please read the [API documentation](https://api.docs-dispatcher.io/docs), also available as [Swagger format](https://api.docs-dispatcher.io/docs/swagger/).

#### ServiceMediator

A single class that will be in charge to temporary store your defined services and their corresponding configurations in order to build the request to be sent to the API.

#### Client

The class that actually triggers calls to API. It decorates a `GuzzleHttp\Client` instance.

### Usage

A realistic usage can be found in the attached sample file `exemple.php`. For more details about configuring each services, please refer to the API documentation.
