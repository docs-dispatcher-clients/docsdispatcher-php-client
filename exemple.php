<?php

use DocsDispatcherIo\Sdk;

require __DIR__ . '/vendor/autoload.php';

// 1. Create authentication
$authenticator = new Sdk\Authentication\BasicAuthAuthentication('login', 'password');

// 2. Initialize client
$client = new Sdk\Client($authenticator);

// 3. Configure service(s). See API documentation for restrictions
$requestFactory = (new Sdk\ServiceMediator())
    ->addService(
        (new Sdk\Service\FileService('my-template'))
            ->addTarget(new Sdk\Argument\Target\ZohoCRMUploadTarget('Leads', 'zcrm_124124'))
            ->addTarget(new Sdk\Argument\Target\ZohoCRMUploadTarget('Contacts', 'zcrm_124124'))
    )
    ->composeWithService(new Sdk\Service\UploadService());

// 4. Execute request
try {
    echo $client->executeRequest($requestFactory, 'text/html')->getBody()->getContents();
    // If service(s) support(s) output into a file, the following line is enough to create the file
    // $client->executeRequest($requestFactory, 'application/pdf');
} catch (\Exception $e) {
    echo "Error occured:" . $e;
}
