<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk;

use DocsDispatcherIo\Sdk\Authentication\AuthenticationInterface;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientInterface as GuzzleHttpClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class Client
{
    /** @var string */
    private $baseUrl = 'https://api.docs-dispatcher.io/api';

    /**
     * @var AuthenticationInterface
     */
    protected $authentication;

    public function __construct(AuthenticationInterface $authentication)
    {
        $this->authentication = $authentication;
    }

    /**
     * @throws \InvalidArgumentException If no service has been registered.
     * @throws \Throwable                If an invalid response is received.
     */
    public function executeRequest(ServiceMediator $serviceMediator, string $accept): ResponseInterface
    {
        if (!\count($serviceMediator)) {
            throw new \InvalidArgumentException('You must register at least 1 service.');
        }

        $requestPath = $serviceMediator->getEndpoint();
        $payload = $serviceMediator->getPayload();
        $guzzleClient = $this->buildHttpClient();
        $options = $this->buildOptionsFromPayload($payload, $accept);
        $this->authentication->authenticate($guzzleClient, $requestPath, $options);

        try {
            $response = $guzzleClient->post($requestPath, $options);
        } catch (ClientException $clientException) {
            throw $this->normalizeClientException($clientException);
        }

        return $response;
    }

    /**
     * Override Api entrypoint.
     */
    public function setBaseUrl(string $url)
    {
        $this->baseUrl = $url;
    }

    protected function buildHttpClient(): GuzzleHttpClientInterface
    {
        return new GuzzleClient([
            'base_uri' => $this->baseUrl,
            'headers' => [
                'User-Agent' => 'DocsDispatcher PHP SDK',
            ],
        ]);
    }

    protected function buildOptionsFromPayload(array $payload, string $accept): array
    {
        $options = [
            RequestOptions::HEADERS => [
                'Accept' => $accept,
            ],
            RequestOptions::JSON => $payload,
        ];

        if (isset($payload['resultFileName'])) {
            $options[RequestOptions::SINK] = $payload['resultFileName'];
        }

        return $options;
    }

    protected function normalizeClientException(ClientException $clientException): \Throwable
    {
        switch ($clientException->getCode()) {
            case 400:
            case 409:
            case 417:
            case 422:
                return new Exception\BadRequestException($clientException);
            case 415:
                return new Exception\UnsupportedMediaTypeException($clientException);
            case 401:
            case 403:
                return new Exception\AuthenticationFailedException($clientException);
            case 404:
                return new Exception\NotFoundException($clientException);
        }
    }
}
