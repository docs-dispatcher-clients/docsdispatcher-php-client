<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Exception;

use GuzzleHttp\Exception\ClientException;

/**
 * @method ClientException getPrevious
 */
class NotFoundException extends \InvalidArgumentException
{
    public function __construct(ClientException $clientException)
    {
        parent::__construct($clientException->getMessage(), $clientException->getCode(), $clientException);
    }
}
