<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk;

use DocsDispatcherIo\Sdk\Service\ComposableServiceInterface;
use DocsDispatcherIo\Sdk\Service\ServiceInterface;

class ServiceMediator implements \Countable
{
    /**
     * @var ServiceInterface[]
     */
    protected $services = [];

    /**
     * @param ServiceInterface[]|null $services
     */
    public function __construct(?array $services = [])
    {
        $this->services = $services;
    }

    public function count(): int
    {
        return \count($this->services);
    }

    public function addService(ServiceInterface $service): self
    {
        $this->services[] = $service;

        return $this;
    }

    public function composeWithService(ComposableServiceInterface $service): self
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * @return ServiceInterface[]
     */
    public function getServices(): array
    {
        return $this->services;
    }

    public function getEndpoint(): string
    {
        if (!\count($this)) {
            throw new \RuntimeException('You must register at least one service to be able to generate the call\'s endpoint.');
        }

        $endpoint = '/api';

        foreach ($this->services as $service) {
            $endpoint .= '/'.$service->getEndpointName();
        }

        return $endpoint;
    }

    public function getPayload(): array
    {
        if (!\count($this)) {
            throw new \RuntimeException('You must register at least one service to be able to generate the request\'s payload.');
        }

        $payload = [];

        foreach ($this->services as $service) {
            $payload += $service->buildPayload();
        }

        return $payload;
    }
}
