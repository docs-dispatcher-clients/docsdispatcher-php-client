<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk;

interface RequestableInterface
{
    /**
     * Build the payload based on its parameters.
     */
    public function buildPayload(): array;
}
