<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Service;

use DocsDispatcherIo\Sdk\Argument\Address;
use DocsDispatcherIo\Sdk\Argument\Attachment;
use DocsDispatcherIo\Sdk\Traits\MixedPropertyTrait;
use DocsDispatcherIo\Sdk\Traits\WithDataTrait;
use DocsDispatcherIo\Sdk\Traits\WithTargetsTrait;

class PostalService implements ServiceInterface
{
    use MixedPropertyTrait;
    use WithDataTrait;
    use WithTargetsTrait;

    /**
     * @var string|null
     */
    protected $provider;

    /**
     * @var string|null
     */
    protected $subject;

    /**
     * @var Address|null
     */
    protected $sender;

    /**
     * @var Address[]
     */
    protected $receivers;

    /**
     * @var Attachment[]
     */
    protected $documents;

    /**
     * @var string|null {@see \DocsDispatcherIo\Sdk\Argument\Enums\EnvelopeFormats}
     */
    protected $envelopeFormat;

    /**
     * @var string|null {@see \DocsDispatcherIo\Sdk\Argument\Enums\ColorModes}
     */
    protected $colorMode;

    /**
     * @var string|null {@see \DocsDispatcherIo\Sdk\Argument\Enums\PostageTypes}
     */
    protected $postage;

    /**
     * @var bool|null
     */
    protected $bothSides;

    /**
     * @var string|null
     */
    protected $targetFilename;

    /**
     * @var array|null
     */
    protected $settings;

    /**
     * @param Address|Address[]       $receivers
     * @param Attachment|Attachment[] $documents
     */
    public function __construct(Address $sender, $receivers, $documents)
    {
        $this->sender = $sender;
        $this->receivers = $this->makeIterable($receivers);
        $this->documents = $this->makeIterable($documents);
    }

    public function getEndpointName(): string
    {
        return 'postal';
    }

    public function buildPayload(): array
    {
        $payload = [
            'sender' => $this->sender->buildPayload(),
            'receivers' => [],
            'documents' => [],
        ];

        $this->buildPayloadData($payload);

        foreach ($this->receivers as $receiver) {
            $payload['receivers'][] = $receiver->buildPayload();
        }

        foreach ($this->documents as $document) {
            $payload['documents'][] = $document->buildPayload();
        }

        if ($this->provider) {
            $payload['provider'] = $this->provider;
        }

        if ($this->subject) {
            $payload['subject'] = $this->subject;
        }

        if ($this->envelopeFormat) {
            $payload['envelopeFormat'] = $this->envelopeFormat;
        }

        if ($this->colorMode) {
            $payload['colorMode'] = $this->colorMode;
        }

        if ($this->postage) {
            $payload['postage'] = $this->postage;
        }

        if ($this->bothSides) {
            $payload['bothSides'] = $this->bothSides;
        }

        if ($this->targetFilename) {
            $payload['targetFilename'] = $this->targetFilename;
        }

        if (\is_array($this->settings)) {
            $payload['settings'] = $this->settings;
        }

        return $this->buildPayloadTargets($payload);
    }

    public function addReceiver(Address $receiver): self
    {
        $this->receivers[] = $receiver;

        return $this;
    }

    public function addDocument(Attachment $document): self
    {
        $this->documents[] = $document;

        return $this;
    }

    public function setProvider(?string $provider = null): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function setSubject(?string $subject = null): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function setEnvelopeFormat(?string $envelopeFormat = null): self
    {
        $this->envelopeFormat = $envelopeFormat;

        return $this;
    }

    public function setColorMode(?string $colorMode = null): self
    {
        $this->colorMode = $colorMode;

        return $this;
    }

    public function setPostage(?string $postage = null): self
    {
        $this->postage = $postage;

        return $this;
    }

    public function setBothSides(?bool $bothSides = null): self
    {
        $this->bothSides = $bothSides;

        return $this;
    }

    public function setTargetFilename(?string $targetFilename = null): self
    {
        $this->targetFilename = $targetFilename;

        return $this;
    }

    public function setSettings(?array $settings = []): self
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * @param int|string $key
     */
    public function addSetting($key, $value): self
    {
        if (!\is_array($this->settings)) {
            $this->settings = [];
        }
        $this->settings[$key] = $value;

        return $this;
    }
}
