<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Service;

use DocsDispatcherIo\Sdk\Argument\Attachment;
use DocsDispatcherIo\Sdk\Traits\MixedPropertyTrait;
use DocsDispatcherIo\Sdk\Traits\WithDataTrait;

class EmailService implements ServiceInterface
{
    use MixedPropertyTrait;
    use WithDataTrait;

    /**
     * @var array|string|null
     */
    private $from;

    /**
     * @var array
     */
    private $to;

    /**
     * @var string|null
     */
    private $subject;

    /**
     * @var string|null
     */
    private $body;

    /**
     * @var array|null
     */
    private $cc;

    /**
     * @var array|null
     */
    private $bcc;

    /**
     * @var string|null
     */
    private $templateName;

    /**
     * @var Attachment[]|null
     */
    private $attachments;

    /**
     * @param array|string $from
     * @param array|string $to
     */
    public function __construct($from, $to, ?string $subject = null, ?string $body = null)
    {
        $this->from = $from;
        $this->to = $this->makeIterable($to, false);
        $this->subject = $subject;
        $this->body = $body;
    }

    public function getEndpointName(): string
    {
        return 'email';
    }

    public function buildPayload(): array
    {
        $payload = [
            'to' => $this->to,
        ];

        if ($this->from) {
            if (\is_array($this->from) && $this->from['name'] && $this->from['mail']) {
                $payload['from'] = ['name' => $this->from['name'], 'mail' => $this->from['mail']];
            } elseif (\is_array($this->from) && $this->from['mail']) {
                $payload['from'] = $this->from['mail'];
            } elseif (\is_string($this->from)) {
                $payload['from'] = $this->from;
            }
        }

        if ($this->subject) {
            $payload['subject'] = $this->subject;
        }

        if ($this->body) {
            $payload['body'] = $this->body;
        }

        if (\is_array($this->cc)) {
            $payload['cc'] = $this->cc;
        }

        if (\is_array($this->bcc)) {
            $payload['bcc'] = $this->bcc;
        }

        if ($this->templateName) {
            $payload['templateName'] = $this->templateName;
        }

        if ($this->attachments) {
            $payload['attachments'] = [];

            foreach ($this->attachments as $attachment) {
                $payload['attachments'][] = $attachment->buildPayload();
            }
        }

        return $this->buildPayloadData($payload);
    }

    public function setSubject(?string $subject = null): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function setBody(?string $body = null): self
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @param string|string[] $cc
     */
    public function setCc($cc = null): self
    {
        $this->cc = $this->makeIterable($cc);

        return $this;
    }

    public function addCc(string $cc): self
    {
        if (!\is_array($this->cc)) {
            $this->cc = [];
        }

        $this->cc[] = $cc;

        return $this;
    }

    /**
     * @param string|string[] $bcc
     */
    public function setBcc($bcc = null): self
    {
        $this->bcc = $this->makeIterable($bcc);

        return $this;
    }

    public function addBcc(string $bcc): self
    {
        if (!\is_array($this->bcc)) {
            $this->bcc = [];
        }

        $this->bcc[] = $bcc;

        return $this;
    }

    public function setTemplateName(?string $templateName = null): self
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * @param Attachment[]|null $attachments
     */
    public function setAttachments(?array $attachments = null): self
    {
        $this->attachments = $attachments;

        return $this;
    }

    public function addAttachment(Attachment $attachment): self
    {
        if (!\is_array($this->attachments)) {
            $this->attachments = [];
        }

        $this->attachments[] = $attachment;

        return $this;
    }
}
