<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Service;

use DocsDispatcherIo\Sdk\Argument\AbstractTarget;
use DocsDispatcherIo\Sdk\Traits\WithDataTrait;
use DocsDispatcherIo\Sdk\Traits\WithTargetsTrait;

abstract class AbstractTargetableService implements ServiceInterface
{
    use WithDataTrait;
    use WithTargetsTrait;

    /**
     * @var string|null
     */
    protected $templateName;

    /**
     * @var string|null
     */
    protected $resultFileName;

    /**
     * @param AbstractTarget[]|null $targets
     */
    public function __construct(
        ?string $templateName = null,
        ?array $data = null,
        ?string $resultFileName = null,
        ?array $targets = null
    ) {
        $this->templateName = $templateName;
        $this->targets = $targets;
        $this->data = $data;
        $this->resultFileName = $resultFileName;
    }

    public function buildPayload(): array
    {
        $payload = [];

        if (null !== $this->templateName) {
            $payload['templateName'] = $this->templateName;
        }

        $this->buildPayloadData($payload);

        if (null !== $this->resultFileName) {
            $payload['resultFileName'] = $this->resultFileName;
        }

        return $this->buildPayloadTargets($payload);
    }

    public function setResultFileName(?string $resultFileName = null): self
    {
        $this->resultFileName = $resultFileName;

        return $this;
    }
}
