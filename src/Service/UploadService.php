<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Service;

class UploadService extends AbstractTargetableService implements ComposableServiceInterface
{
    public function getEndpointName(): string
    {
        return 'upload';
    }
}
