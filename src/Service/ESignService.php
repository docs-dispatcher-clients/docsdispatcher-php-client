<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Service;

use DocsDispatcherIo\Sdk\Argument\BasicRequest;
use DocsDispatcherIo\Sdk\Argument\ESignField;
use DocsDispatcherIo\Sdk\Argument\ESignFileContentRequest;
use DocsDispatcherIo\Sdk\Argument\Recipient;
use DocsDispatcherIo\Sdk\Traits\MixedPropertyTrait;
use DocsDispatcherIo\Sdk\Traits\WithDataTrait;
use DocsDispatcherIo\Sdk\Traits\WithTargetsTrait;

class ESignService implements ServiceInterface
{
    use MixedPropertyTrait;
    use WithDataTrait;
    use WithTargetsTrait;

    /**
     * @var string|null
     */
    protected $provider;

    /**
     * @var string|null
     */
    protected $subject;

    /**
     * @var string|null
     */
    protected $messageBody;

    /**
     * @var BasicRequest|null
     */
    protected $message;

    /**
     * @var string|null
     */
    protected $finalDocMessageSubject;

    /**
     * @var string|null
     */
    protected $finalDocMessageBody;

    /**
     * @var BasicRequest|null
     */
    protected $finalDocMessage;

    /**
     * @var Recipient[]
     */
    protected $recipients;

    /**
     * @var ESignFileContentRequest[]
     */
    protected $documents;

    /**
     * @var Recipient[]|null
     */
    protected $finalDocRecipients;

    /**
     * @var string|null {@see \DocsDispatcherIo\Sdk\Argument\Enums\ESignTypes}
     */
    protected $type;

    /**
     * @var string|null {@see \DocsDispatcherIo\Sdk\Argument\Enums\ESignDeliveryTypes}
     */
    protected $deliveryType;

    /**
     * @var string|null {@see \DocsDispatcherIo\Sdk\Argument\Enums\ESignModes}
     */
    protected $signingMode;

    /**
     * @var int|null
     */
    protected $expirationTime;

    /**
     * @var ESignField[]|null
     */
    protected $fields;

    /**
     * @var array|null
     */
    protected $settings;

    /**
     * @param Recipient|Recipient[]                             $recipients
     * @param ESignFileContentRequest|ESignFileContentRequest[] $documents
     */
    public function __construct($recipients, $documents)
    {
        $this->recipients = $this->makeIterable($recipients);
        $this->documents = $this->makeIterable($documents);
    }

    public function getEndpointName(): string
    {
        return 'esign';
    }

    public function buildPayload(): array
    {
        $payload = [
            'recipients' => [],
            'documents' => [],
        ];
        $this->buildPayloadData($payload);

        foreach ($this->recipients as $recipient) {
            $payload['recipients'][] = $recipient->buildPayload();
        }

        foreach ($this->documents as $document) {
            $payload['documents'][] = $document->buildPayload();
        }

        if ($this->provider) {
            $payload['provider'] = $this->provider;
        }

        if ($this->subject) {
            $payload['subject'] = $this->subject;
        }

        if ($this->messageBody) {
            $payload['messageBody'] = $this->messageBody;
        }

        if ($this->message) {
            $payload['message'] = $this->message->buildPayload();
        }

        if ($this->finalDocMessageSubject) {
            $payload['finalDocMessageSubject'] = $this->finalDocMessageSubject;
        }

        if ($this->finalDocMessageBody) {
            $payload['finalDocMessageBody'] = $this->finalDocMessageBody;
        }

        if ($this->finalDocMessage) {
            $payload['finalDocMessage'] = $this->finalDocMessage->buildPayload();
        }

        if (\is_array($this->finalDocRecipients)) {
            $payload['finalDocRecipients'] = [];

            foreach ($this->finalDocRecipients as $recipient) {
                $payload['finalDocRecipients'][] = $recipient->buildPayload();
            }
        }

        if ($this->type) {
            $payload['type'] = $this->type;
        }

        if ($this->deliveryType) {
            $payload['deliveryType'] = $this->deliveryType;
        }

        if ($this->signingMode) {
            $payload['signingMode'] = $this->signingMode;
        }

        if (null !== $this->expirationTime && $this->expirationTime >= 0) {
            $payload['expirationTime'] = $this->expirationTime;
        }

        if (\is_array($this->fields)) {
            $payload['fields'] = [];

            foreach ($this->fields as $field) {
                $payload['fields'][] = $field->buildPayload();
            }
        }

        if (\is_array($this->settings)) {
            $payload['settings'] = $this->settings;
        }

        return $this->buildPayloadTargets($payload);
    }

    public function addRecipient(Recipient $recipient): self
    {
        $this->recipients[] = $recipient;

        return $this;
    }

    public function addDocument(ESignFileContentRequest $document): self
    {
        $this->documents[] = $document;

        return $this;
    }

    public function setProvider(?string $provider = null): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function setFixedMessage(?string $subject = null, ?string $messageBody = null): self
    {
        $this->subject = $subject;
        $this->messageBody = $messageBody;

        return $this;
    }

    public function setMessage(?BasicRequest $message = null): self
    {
        $this->message = $message;

        return $this;
    }

    public function setFixedFinalDocMessage(
        ?string $finalDocMessageSubject = null,
        ?string $finalDocMessageBody = null
    ): self {
        $this->finalDocMessageSubject = $finalDocMessageSubject;
        $this->finalDocMessageBody = $finalDocMessageBody;

        return $this;
    }

    public function setFinalDocMessage(?BasicRequest $finalDocMessage = null): self
    {
        $this->finalDocMessage = $finalDocMessage;

        return $this;
    }

    /**
     * @param Recipient|Recipient[] $finalDocRecipients
     */
    public function setFinalDocRecipients($finalDocRecipients): self
    {
        $this->finalDocRecipients = $this->makeIterable($finalDocRecipients);

        return $this;
    }

    /**
     * @param ESignService $finalDocRecipient
     */
    public function addFinalDocRecipient(Recipient $finalDocRecipient): self
    {
        if (!\is_array($this->finalDocRecipients)) {
            $this->finalDocRecipients = [];
        }
        $this->finalDocRecipients[] = $finalDocRecipient;

        return $this;
    }

    public function setType(?string $type = null): self
    {
        $this->type = $type;

        return $this;
    }

    public function setDeliveryType(?string $deliveryType = null): self
    {
        $this->deliveryType = $deliveryType;

        return $this;
    }

    public function setSigningMode(?string $signingMode = null): self
    {
        $this->signingMode = $signingMode;

        return $this;
    }

    public function setExpirationTime(?int $expirationTime = null): self
    {
        $this->expirationTime = $expirationTime;

        return $this;
    }

    /**
     * @param ESignField|ESignField[] $fields
     */
    public function setFields($fields): self
    {
        $this->fields = $this->makeIterable($fields);

        return $this;
    }

    public function addField(ESignField $field): self
    {
        if (!\is_array($this->fields)) {
            $this->fields = [];
        }
        $this->fields[] = $field;

        return $this;
    }

    public function setSettings(?array $settings = []): self
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * @param int|string $key
     */
    public function addSetting($key, $value): self
    {
        if (!\is_array($this->settings)) {
            $this->settings = [];
        }
        $this->settings[$key] = $value;

        return $this;
    }
}
