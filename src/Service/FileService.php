<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Service;

class FileService extends AbstractTargetableService implements ServiceInterface
{
    /**
     * @param AbstractTarget[]|null $targets
     */
    public function __construct(
        string $templateName,
        ?array $data = null,
        ?string $resultFileName = null,
        ?array $targets = null
    ) {
        parent::__construct($templateName, $data, $resultFileName, $targets);
    }

    public function getEndpointName(): string
    {
        return 'file';
    }
}
