<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Authentication;

use GuzzleHttp\ClientInterface as GuzzleHttpClientInterface;

interface AuthenticationInterface
{
    /**
     * Perform required operations or add parameters to perform authentication.
     * Note that $endpointUrl and $options are passed by reference so you can also alter them if necessary.
     */
    public function authenticate(GuzzleHttpClientInterface $guzzleClient, string &$endpointUrl, array &$options): void;
}
