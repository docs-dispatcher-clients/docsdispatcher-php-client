<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument\Enums;

class ESignModes
{
    public const SEQUENTIAL = 'SEQUENTIAL';
    public const PARALLEL = 'PARALLEL';
}
