<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument\Enums;

class ESignTypes
{
    public const SIMPLE = 'SIMPLE';
    public const CERTIFIED = 'CERTIFIED';
    public const SMART = 'SMART';
    public const ADVANCED = 'ADVANCED';
}
