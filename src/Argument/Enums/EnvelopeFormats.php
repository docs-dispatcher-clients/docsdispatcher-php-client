<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument\Enums;

class EnvelopeFormats
{
    public const C4 = 'C4';
    public const C5 = 'C5';
    public const C6 = 'C6';
    public const AUTO = 'AUTO';
}
