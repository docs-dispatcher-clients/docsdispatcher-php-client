<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument\Enums;

class ESignRecipientRoles
{
    public const SIGNER = 'SIGNER';
    public const APPROVER = 'APPROVER';
    public const WATCHER = 'WATCHER';
}
