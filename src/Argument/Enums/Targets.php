<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument\Enums;

class Targets
{
    public const GEMA = 'gemaUpload';
    public const ZOHO_CRM = 'zohoCRMUpload';
}
