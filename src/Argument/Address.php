<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument;

use DocsDispatcherIo\Sdk\RequestableInterface;

class Address implements RequestableInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $address1;

    /**
     * @var string|null
     */
    protected $address2;

    /**
     * @var string|null
     */
    protected $address3;

    /**
     * @var string|null
     */
    protected $address4;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $zipCode;

    /**
     * @var string
     */
    protected $countryCode;

    public function __construct(string $name, string $address1, string $city, string $zipCode, string $countryCode)
    {
        $this->name = $name;
        $this->address1 = $address1;
        $this->city = $city;
        $this->zipCode = $zipCode;
        $this->countryCode = $countryCode;
    }

    public function buildPayload(): array
    {
        $payload = [
            'name' => $this->name,
            'address1' => $this->address1,
            'city' => $this->city,
            'zipCode' => $this->zipCode,
            'countryCode' => $this->countryCode,
        ];

        if ($this->address2) {
            $payload['address2'] = $this->address2;
        }

        if ($this->address3) {
            $payload['address3'] = $this->address3;
        }

        if ($this->address4) {
            $payload['address4'] = $this->address4;
        }

        return $payload;
    }

    public function setAddress2(?string $address2 = null): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function setAddress3(?string $address3 = null): self
    {
        $this->address3 = $address3;

        return $this;
    }

    public function setAddress4(?string $address4 = null): self
    {
        $this->address4 = $address4;

        return $this;
    }
}
