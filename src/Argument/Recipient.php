<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument;

use DocsDispatcherIo\Sdk\Argument\Enums\ESignRecipientRoles;
use DocsDispatcherIo\Sdk\RequestableInterface;
use DocsDispatcherIo\Sdk\Traits\MixedPropertyTrait;

class Recipient implements RequestableInterface
{
    use MixedPropertyTrait;

    /**
     * @var string|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $firstname;

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $email;

    /**
     * @var string|null
     */
    protected $phone;

    /**
     * @var string {@see ESignRecipientRoles}
     */
    protected $role;

    /**
     * @var array|null
     */
    protected $targetUsers;

    /**
     * @var array
     */
    protected $options;

    public function __construct(string $role = ESignRecipientRoles::SIGNER, array $options = [])
    {
        $this->role = $role;
        $this->options = $options;
    }

    public function buildPayload(): array
    {
        $payload = [
            'role' => $this->role,
            'options' => $this->options,
        ];

        if ($this->id) {
            $payload['id'] = $this->id;
        }

        if ($this->firstname) {
            $payload['firstname'] = $this->firstname;
        }

        if ($this->name) {
            $payload['name'] = $this->name;
        }

        if ($this->email) {
            $payload['email'] = $this->email;
        }

        if ($this->phone) {
            $payload['phone'] = $this->phone;
        }

        if (\is_array($this->targetUsers)) {
            $payload['targetUsers'] = $this->targetUsers;
        }

        return $payload;
    }

    public function setId(?string $id = null): self
    {
        $this->id = $id;

        return $this;
    }

    public function setFirstname(?string $firstname = null): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function setName(?string $name = null): self
    {
        $this->name = $name;

        return $this;
    }

    public function setEmail(?string $email = null): self
    {
        $this->email = $email;

        return $this;
    }

    public function setPhone(?string $phone = null): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param string|string[] $targetUsers
     */
    public function setTargetUsers($targetUsers = null): self
    {
        $this->targetUsers = $this->makeIterable($targetUsers);

        return $this;
    }

    public function addTargetUser(string $targetUser): self
    {
        if (!\is_array($this->targetUsers)) {
            $this->targetUsers = [];
        }

        $this->targetUsers[] = $targetUser;

        return $this;
    }

    /**
     * @param int|string $key
     */
    public function addOption($key, $value): self
    {
        if (!\is_array($this->options)) {
            $this->options = [];
        }

        $this->options[$key] = $value;

        return $this;
    }
}
