<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument;

use DocsDispatcherIo\Sdk\RequestableInterface;

class ESignField implements RequestableInterface
{
    /**
     * @var string
     */
    protected $recipient;

    /**
     * @var string|null
     */
    protected $anchorKey;

    /**
     * @var string|null
     */
    protected $type;

    /**
     * @var string|null
     */
    protected $additionalOptions;

    /**
     * @var string[]|null
     */
    protected $consents;

    public function __construct(
        string $recipient,
        ?string $anchorKey = null,
        ?string $type = null,
        ?string $additionalOptions = null,
        ?array $consents = null
    ) {
        $this->recipient = $recipient;
        $this->anchorKey = $anchorKey;
        $this->type = $type;
        $this->additionalOptions = $additionalOptions;
        $this->consents = $consents;
    }

    public function buildPayload(): array
    {
        $payload = [
            'recipient' => $this->recipient,
        ];

        if ($this->anchorKey) {
            $payload['anchorKey'] = $this->anchorKey;
        }

        if ($this->type) {
            $payload['type'] = $this->type;
        }

        if ($this->additionalOptions) {
            $payload['additionalOptions'] = $this->additionalOptions;
        }

        if ($this->consents) {
            $payload['consents'] = $this->consents;
        }

        return $payload;
    }

    public function setAnchorKey(?string $anchorKey = null): self
    {
        $this->anchorKey = $anchorKey;

        return $this;
    }

    /**
     * @param type {@see Enums\ESignFieldTypes}
     */
    public function setType(?string $type = null): self
    {
        $this->type = $type;

        return $this;
    }

    public function setAdditionalOptions(?string $additionalOptions = null): self
    {
        $this->additionalOptions = $additionalOptions;

        return $this;
    }

    public function setConsents(?array $consents = []): self
    {
        $this->consents = $consents;

        return $this;
    }

    public function addConsent(string $consent): self
    {
        if (!\is_array($this->consents)) {
            $this->consents = [];
        }

        $this->consents[] = $consent;

        return $this;
    }
}
