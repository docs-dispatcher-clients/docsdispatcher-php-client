<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument\Target;

use DocsDispatcherIo\Sdk\Argument\AbstractTarget;
use DocsDispatcherIo\Sdk\Argument\Enums\Targets;

/**
 * @deprecated
 */
class GemaUploadTarget extends AbstractTarget
{
    /**
     * @var string
     */
    protected $target = Targets::GEMA;
}
