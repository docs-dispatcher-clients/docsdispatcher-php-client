<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument;

use DocsDispatcherIo\Sdk\RequestableInterface;

abstract class AbstractTarget implements RequestableInterface
{
    /**
     * @var string
     */
    protected $target;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $id;

    public function __construct(string $type, string $id)
    {
        $this->type = $type;
        $this->id = $id;
    }

    public function buildPayload(): array
    {
        return [
            'target' => $this->target,
            'type' => $this->type,
            'id' => $this->id,
        ];
    }
}
