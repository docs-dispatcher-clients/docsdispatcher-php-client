<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Traits;

trait WithDataTrait
{
    /**
     * @var array|null
     */
    protected $data;

    public function setData(?array $data = null): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param int|string $key
     */
    public function addDatum($key, $value): self
    {
        if (!\is_array($this->data)) {
            $this->data = [];
        }

        $this->data[$key] = $value;

        return $this;
    }

    protected function buildPayloadData(array &$payload): array
    {
        if ($this->data) {
            $payload['data'] = $this->data;
        }

        return $payload;
    }
}
