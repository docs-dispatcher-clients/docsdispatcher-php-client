<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Traits;

use DocsDispatcherIo\Sdk\Argument\AbstractTarget;

trait WithTargetsTrait
{
    /**
     * @var AbstractTarget[]|null
     */
    protected $targets;

    /**
     * @param AbstractTarget[]|null $targets
     */
    public function setTargets(?array $targets = null): self
    {
        $this->targets = $targets;

        return $this;
    }

    public function addTarget(AbstractTarget $target): self
    {
        if (!\is_array($this->targets)) {
            $this->targets = [];
        }

        $this->targets[] = $target;

        return $this;
    }

    protected function buildPayloadTargets(array &$payload): array
    {
        if (\is_array($this->targets)) {
            $payload['targets'] = [];

            foreach ($this->targets as $target) {
                $payload['targets'][] = $target->buildPayload();
            }
        }

        return $payload;
    }
}
